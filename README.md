# CriptoExchangesVe
El 26 de abril de 2018, Nicolás Maduro, anunció la certificación de 16 casas de cambio de criptomonedas, conocidas como “exchanges”, en el marco del lanzamiento del con el objetivo de garantizar el nivel de transacciones de “El Petro”. También, con el objetivo familiarizar a los venezolanos con estas casas de cambio de cripto-activos (https://youtu.be/FqAcVi9U79I)

Esta lista responde a la necesidad de conocer las casas de cambio que operan con criptomonedas y Bolivares venezolanos. Además de aportar al crecimiento y adopción de las criptomonedas en el pais.

## 16 casas de cambio autorizadas

| Casa de cambio                       | Monedas       | Link        | Observaciones   |
|------------------------------------- |---------------|-------------|-----------------|
| Cripto Exchange                      | Desconocido   | Desconocido | Sin información ni redes. |
| Cripto Capital                       | Desconocido   | Desconocido | Sin información ni redes. |
| Asesoría Financiera C.A.             | Desconocido   | Desconocido | Sin información ni redes. |
| Digitalcambio                        | Desconocido   | Desconocido | Sin información ni redes. |
| Amberes Coin                         | BTC, ETH, XRP, PTR | [![Página][web]](http://www.amberescoin.com/) | Abrieron los registros de usuarios el 01/07/2018. Poca navegabilidad en su portal web. |
| Cave Blockchain                      | BTC, BCH, BTG, LTC, ZCASH, PTR   | [![Página][web]](http://www.caveblockchain.com) | Sin información sobre su plataforma. Sin publicaciones ni seguidores en sus redes sociales. ENVIAN USUARIO Y CONTRASEÑA AL CORREO ELECTRONICO, EN TEXTO PLANO. |
| Valoratta Casa de Bolsa              | Desconocido   | Desconocido | Sin información ni redes. |
| Coinsecure.ve                        | BTC, PTR, INR | Desconocido | Aparentemente relacionada con la casa de cambio en India, [CoinSecure](https://coinsecure.in/) (sin operar desde un robo de BTC en abril 2018), afirma que agregarán PTR a su sistema de trade |
| Cryptia exchange                     | BTC, LTC, XRP, ETH, DASH, EUR   | [![Página][web]](https://www.cryptiaexchange.com) | El RIF J411121649 pertenece a CRYPTO EXCHANGE VENEZUELA CA, curiosamente una casa de cambio en la lista. Aún no se ha confirmado si tienen relación. |
| Criptolago                           | BTC, PTR      | [![Página][web]](http://exchange.criptolago.com.ve) | Granja de mineria y criptomoneda (en desarrollo) |
| Criptoven trade C.A.                 | PTR, BTC, ETH | [![Página][web]](https://criptoven.exchange/) | Se desconoce desde cuando está operando. Tiene muy poca actividad en sus redes sociales.  |
| Criptoactivo Menex C.A.              | BTC, LTC, DASH | [![Página][web]](https://www.menex.io/) | El 27/08/2018 lanzaron la versión beta de su plataforma. |
| Criptoactivo Bancar C.A.             | PTR, BTC, ETH, XEM | [![Página][web]](http://www.bancarexchange.io) | Link de registro y login roto. |
| Criptomundo Casa de Intercambio C.A. | Desconocido   | Desconocido | Sin información ni redes. |
| Inversiones Financieras 1444 C.A.    | Desconodido   | [![Página][web]](http://veinte.net/) | Sin información sobre su plataforma de intercambio.  |
| Criptoactivo Criptoes                | Desconocido   | Desconocido | Sin información ni redes. |
| Afx Trade*                           | PTR, BTC, LTC, ETH, DASH   | [![Página][web]](https://afx.trade/) | Sin redes sociales. No posee enlace para crear cuenta nueva, pero si para iniciar sesión. El sistema aún está en desarrollo. |
| Sunacrip**                           | PTR           |  | De acuerdo con [Criptonoticias](https://www.criptonoticias.com/mercado-cambiario/petro-cotiza-cinco-precios-entidades-bancarias-venezolanas/), el 12/11/2018 fue habilitada la compra de PTR, únicamente en las oficinas de la Sunacrip, en Caracas. Aún no hay información sobre la venta de PTR. |

 *No fue anunciada en abril, pero es otra casas de cambio autorizada   
**No hay información sobre la compra/venta de BTC

## Otras casas de cambio en VEF/VES

| Casa de cambio | Monedas     | Link                      | Observaciones                                                                  |
|----------------|-------------|---------------------------|--------------------------------------------------------------------------------|
| Colibit        | DASH, BTC   | [![Página][web]](https://www.colibit.io/) [![Página][web]](http://colibit.exchange/) | Dejó de operar el 31/06/2018 luego de dos meses de presunto bloqueo de CONATEL |
| Monkeycoin     | BTC, BCH, ETH, LTC, SMARTCASH  | [![Página][web]](http://monkeycoin.com.ve/) [![Página][web]](http://monkeycoin.exchange/) | Dejó de operar el 19/08/2018. Aún no está claro cual es el problema que les afecta |
| Italcambio*    |             | [![Página][web]](http://www.italcambio.com) | Italcambio no trabaja con criptomonedas. Y no lo considera a futuro.           |
| Bitinka        | BTC, BCH, BTG, NEO, DASH, ETH, LTC, NANO, ACT, SSC, SSP, XRP, GAS, VEX | [![Página][web]](https://www.bitinka.com)                               | Actualmente, no está disponible. Antes, permitia operar con VEF/VES-BTC, sin embargo no habia mucha oferta/demanada.  |
| AirTM          | BTC, LTC, BCH, ETH, XRP,ZEV, USDT  | [![Página][web]](https://www.airtm.io)      | No investigado                                                                 |
| RapidCambio    |             | [![Página][web]](https://rapidcambio.com)       | No investigado (en construcción). |
| Cryxto         | XRP         | [![Página][web]](https://cryxto.com)        | El 15/05/2018 suspendieron operaciones a la espera de que se oficialice las licencias y reglas por parte de SUPCACVEN.|
| Basichange     | BTC, ETH, DOGE, DASH LTC, XMR, BCH, BCN, XRP, XEM, ZEC, ETC, ZEN, SMART, BLK, USDT| [![Página][web]](https://basichange.com/) | No investigado|
| ccoins         | BTC, BCH, DAI, ETH, LTC | [![Página][web]](https://ccoins.io/)        | Activo desde el 18/05/19. No investigado, pero parece ser una plataforma P2P.  |
| SurBitcoin     | BTC         | [![Página][web]](https://surbitcoin.com/)   | Primer mercado de Bitcoins en VEF/VES (Sin investigar la actualidad de la plataforma) |
| Cryptolifex    | DASH        | No investigado            | No investigado |
| Orionx         | BTC, ETH, XRP, XLM, DAI, LTC, CHA, LUK, BCH, DASH | [![Página][web]](https://orionx.com) | No investigado. Menciona a Venezuela entre los paises en los que están presentes. Sin embargo, en este momento no disponen de depositos/retiros en moneda fiat. [Anuncio](https://twitter.com/orionx/status/1067502978003693568?ref_src=twsrc%5Etfw) |
| Cryptolocal     | EOS, STEEM, SBD, PSO, KARMA | [![Página][web]](https://cryptolocal.exchange/)   | No investigado. [Tutorial de uso](https://busy.org/@cryptolocal/tutorial-como-usar-cryptolocal) |
| SatoshiTango    | BTC, ETH, BCH, LTC, DAI, XRP | [![Página][web]](https://www.satoshitango.com)   | No investigado. [Abre operaciones en Venezuela](https://twitter.com/SatoshiTango/status/1239529598620241921?s=20) |
| PandaBTM    | BTC, DASH, BCH, DAI | [![Página][web]](https://app.pandabtm.io/)   | No investigado. [Comprar criptomonedas en Venezuela](https://www.criptotendencias.com/actualidad/pandabtm-estrena-su-plataforma-online-para-la-compra-de-criptomonedas-en-venezuela/) |

*Italcambio aparece en varios portales de noticias como una de las 16 casas de cambio autorizadas. Sin embargo, no opera con ninguna criptomoneda.

## Alternativas a Casas de cambio

| Plataforma       | Monedas | Link                      | Observaciones   |
|------------------|---------|---------------------------|-----------------|
| Localbitcoin     | BTC     | [![Página][web]](https://localbitcoins.com) | [Venezuela primer lugar en latinoamerica en intercambio de BTC](https://www.criptonoticias.com/mercado-cambiario/venezuela-primer-lugar-latinoamerica-intercambio-localbitcoins/) |
| LocalCryptos([Localethereum](https://blog.localcryptos.com/announcing-localcryptos/))    | ETH, BTC     | [![Página][web]](https://localcryptos.com/) | [Venezuela con mayor numero de transacciones](https://www.criptonoticias.com/mercado-cambiario/venezuela-pais-mayor-numero-transacciones-usuarios-localethereum/) |
| Paxful           | BTC     | [![Página][web]](https://paxful.com)        | [Dejó de operara en Venezuela](https://twitter.com/paxful_LATAM/status/1305569635933319170)  |
| Liberalcoins     | BTC, LTC, XMR, ZEC, DASH | [![Página][web]](https://liberalcoins.com) | Hay muy poca oferta/demanda en VEF/VES |
| LocalMonero      | XMR     | [![Página][web]](http://localmonero.co)    | La plataforma protege la identidad en las transacciones y ofrece muchos metodos de pago, icluyendo transferencia bancaria. Sin embargo, hay mas ofertas de venta que de compra. [Tutorial](https://youtu.be/6d1W9Pb2PLo)   |
| CryptoWay        | BTC, LTC, ETH, DASH, DOGE, BCH | [![Página][web]](https://cryptoway.io) | Hay bastante oferta/demanda. Aunque comparada con otros, tiene bastante altas las ofertas. Excelente interfaz. |
| Localbitcoincash | BCH     | [![Página][web]](https://localbitcoincash.org) | No investigado |
| Tradebay         | BTC, LTC, DASH, BCH | [![Página][web]](https://tradebay.com/) | No investigado |
| WeSend           | SDT (token)| [![Página][web]](https://www.wesend.co)  | No investigado. Tiene su propio token para realizar los intercambios en el mercado que ofrece. Las transferencias y la tarjeta de crédito son algunos de sus méétodos de pago. | 
| Localcoinswap    |  | [![Página][web]](https://localcoinswap.com) | No investigado |
| HodlHodl     | BTC, PTR | [![Página][web]](https://hodlhodl.com/)   | Tiene muy pocas ofertas de compra/venta, tanto para BTC como para PTR | 
| Binance P2P     | BTC, ETH, EOS, BNB, USDT, BUSD | [![Página][web]](https://p2p.binance.com/)   | Tiene muy pocas ofertas de compra/venta, tanto para BTC como para PTR | 

## Casas de cambio P2P para intercambios en efectivo / en persona
[#P2P exchanges](https://www.reddit.com/r/Bitcoin/comments/b2wj6k/)

## Casas de cambio descentralizadas (DEX) 
[#InDEX](https://github.com/distribuyed/index)

## Pasarelas de Pago

| Plataforma       | Monedas | Link                      | Observaciones   |
|------------------|---------|---------------------------|-----------------|
| Xpay | BTC | [![Página][web]](https://xpay.cash/) | Recientemente anunciaron que se pueden hacer retiros en VES |
| Cryptobuyer | BTC, DASH, LTC | [![Página][web]](https://cryptobuyer.io) | Por investigar |

---

### Donaciones BTC: `1CnJMeQFEcBsX8qmmmcRc5rKXqJ6cNPppc`

---

_Descargo de responsabilidad_: No recomiendo ni respondo por ninguna de las casas de cambio mencionadas. Este contenido fue preparado unicamente para propositos informativos y no debe usarse para tomar desiciones, transacciones ni estrategias de inversión. La intención de esta lista responde a la necesidad de conocer las casas de cambio anunciadas por el presidente y la poca información que hay de ellas. Además de contar con un humilde registro de casas de cambio que operan u operaron con criptomonedas y VES. No pretendo emitir juicio sobre cómo operan ni de sus propietarios. Para operar en alguna de esas plataformas, investigue, asegurese de que es confiable y que entiende las implicaciones de su uso.

## Licencia

[![Public Domain Mark](http://i.creativecommons.org/p/mark/1.0/88x31.png)](http://creativecommons.org/publicdomain/mark/1.0/)

This work is free of known copyright restrictions.

[web]: https://icongr.am/jam/world.svg?size=25 "Website"
